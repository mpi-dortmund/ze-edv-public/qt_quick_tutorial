NAME=${1:-qt_quick_tutorial}

conda_exe=mamba
which ${conda_exe} || conda_exe=conda
which ${conda_exe} || exit 1

source ${CONDA_PREFIX}/etc/profile.d/conda.sh
source ${CONDA_PREFIX_1}/etc/profile.d/conda.sh

${conda_exe} create -n ${NAME} qt -c conda-forge -y
conda activate ${NAME}

${conda_exe} install mesa-libgl-cos6-x86_64 -c anaconda -y
ln -rs ${CONDA_PREFIX}/x86_64-conda-linux-gnu/sysroot/usr/lib64/libGL.so{.1,}

echo "Please install the QT Creator or QT Design Studio to adjust the files ;)"
