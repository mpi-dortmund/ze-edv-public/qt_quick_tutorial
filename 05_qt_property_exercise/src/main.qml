import QtQuick 2.9

Item {
    id: my_item
    width: 400
    height: 400
    // Define my own margin property to scale dynamically
    property int margin: width * 0.1

    // clip: true <- Disallows that children are drawn outside the parent window

    // Nested
    Rectangle {
        id: my_background
        // Save the parent in a variable for easier copy / paste
        property var parent_object: my_item
        x: parent_object.margin
        y: parent_object.margin
        // Define my own margin property to scale dynamically
        property int margin: width * 0.1
        height: parent_object.height - 2 * parent_object.margin
        width: parent_object.width - 2 * parent_object.margin
        color: "gray"


        Rectangle {
            id: upper_lightblue
            property var parent_object: my_background
            x: parent_object.margin
            y: parent_object.margin
            // Define my own margin property to scale dynamically
            property int margin: height * 0.33
            height: (parent_object.height - 2 * parent_object.margin) / 2
            width: parent_object.width - 2 * parent_object.margin
            color: "lightblue"

            Rectangle {
                id: upper_white
                property var parent_object: upper_lightblue
                x: parent_object.margin
                y: parent_object.margin
                // Define my own margin property to scale dynamically
                height: parent_object.height - 2 * parent_object.margin
                width: (parent_object.width - 2 * parent_object.margin) / 4
                color: "white"
            }
        }
    }

    // Individual without nesting just by order
    Rectangle {
        id: lower_green
        property var parent_object: my_background
        x: parent_object.x + parent_object.margin
        y: parent_object.y + parent_object.margin + upper_lightblue.height
        // Define my own margin property to scale dynamically
        property int margin: height * 0.33
        height: (parent_object.height - 2 * parent_object.margin) / 2
        width: parent_object.width - 2 * parent_object.margin
        color: "green"
    }

    Rectangle {
        id: lower_blue
        property var parent_object: lower_green
        x: parent_object.x + parent_object.margin + parent_object.width * 0.15
        y: parent_object.y + parent_object.margin
        // Define my own margin property to scale dynamically
        height: parent_object.height - 2 * parent_object.margin
        width: parent_object.width - 2 * parent_object.margin - parent_object.width * 0.15
        color: "blue"
    }

}
