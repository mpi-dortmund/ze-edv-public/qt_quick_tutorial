import QtQuick 2.0

Item {
    width: 600
    height: 115

    Text {
        id: textElement
        x: 50
        y: 25
        text: "Qt Quick"
        font.family: "Helvetica"
        font.pixelSize: 50
    }

    Rectangle {
        x: 50
        y: 75
        // This width is referring to the width of THIS object NOT the parent
        // Even though width is defined afterwards
        // The height is ALWAYS twice the width at ANY given point in time!!!
        // If the width changes on runtime the height changes accordingly
        height: width / 10
        // Use the width of the text element as width
        width: textElement.width
        color: "green"
    }

    TextInput {
        id: textElement2
        x: 350
        y: 25
        text: "Qt Quick"
        font.family: "Helvetica"
        font.pixelSize: 50
    }

    Rectangle {
        x: 350
        y: 75
        // This width is referring to the width of THIS object NOT the parent
        // Even though width is defined afterwards
        // The height is ALWAYS twice the width at ANY given point in time!!!
        // If the width changes on runtime the height changes accordingly
        height: width / 10
        // Use the width of the text element as width
        width: textElement2.width
        color: "green"
    }

}
