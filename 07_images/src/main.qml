import QtQuick 2.0

Rectangle {
    id: root
    width: 800
    height: 400
    color: "#00a3fc"

    Image {
        x: 150
        y: 150
        source: "images/rocket.png"
        // User sourceSize of the Image object
        width: sourceSize.width * 2
        height: sourceSize.height * 2
        // Print to screen afterwards
        Component.onCompleted: console.log(width, height, sourceSize)
    }

    // Async loading of an image
    Image {
        id: image
        width: 400
        height: 150
        source: "https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74393/world.topo.200407.3x5400x2700.jpg"
        fillMode: Image.PreserveAspectFit

        // Show the progress bar during loading of the image from the net
        Rectangle {
            color: "red"
            x: 0
            y: 375
            height: 25
            width: root.width * image.progress
            visible: image.progress != 1
        }
        onStatusChanged: console.log(root.width)
    }

    property int frames: 1
    AnimatedImage {
        id: animation

        x: 400
        y: 0
        width: 400
        height: 400
        source: "images/image-animated.gif"
        onStatusChanged: {
            if (status === Image.Ready) frames = frameCount
        }

    }
}

