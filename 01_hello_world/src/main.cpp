#include <QtQuick>

int main(int argc, char *argv[1]) {
    QGuiApplication app(argc, argv);
    QQuickView view;
    view.setSource(QUrl("src/main.qml"));
    view.show();
    return app.exec();
}

