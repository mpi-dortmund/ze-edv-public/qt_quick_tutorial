#!/usr/bin/bash

cd $(dirname ${0})
# This builds the c++ files
rm -rf build Makefile
qmake src/main.pro
make
./build/main
