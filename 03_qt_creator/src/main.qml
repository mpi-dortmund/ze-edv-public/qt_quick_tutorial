import QtQuick 2.0

Item {
    width: 400
    height: 400

    Rectangle {
        x: 100
        y: 50
        height: 100
        width: foo()
        color: "lightblue"

        function foo () {
            console.log("hello world", height)
            return height * 2
        }
    }


    // This Rectangle overlaps with the first one, since it is defined at a later stage
    Rectangle {
        x: 100
        y: 100
        height: 100
        width: height * 2
        color: "green"

    }

    // This Rectangle overlaps NOT with the first one, because z is defined to -1
    Rectangle {
        x: 100
        y: 150
        z: -1
        height: 100
        width: height * 2
        color: "orange"

    }

}
